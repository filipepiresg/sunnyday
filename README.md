# Sunny Day

### SOLUÇÃO

Projeto criado com o comando `npx react-native init sunnyDay` utilizando o template de typescript na versão `0.63.0`. Seguindo as definições

```md
Desenvolva um aplicativo que consuma a localização atual do usuário e exiba na interface o endereço atual os dados climáticos da região e um botão para atualizar os dados.

Para fazer essa busca, pode-se usar a API do Open Weather Map: http://api.openweathermap.org
```

Roda em dispositivos Apple (iOS) e em Android.

### FUNCIONAMENTO

Ao abrir o app o app, ele vai pedir permissão de localizacão, e assim que garantido, ele faz duas chamadas a API do OpenWeather, uma para pegar os dados atuais, e a segunda para pegar os dados de mínima e máxima dos próximos dias. E mostra em uma lista vertical.

No fim da tela, temos o nome da sua cidade e ao lado, temos o botão de refresh. Que vai fazer a requisicão a API, novamente.

### INSTALAÇÃO

Requisitos:

- [NodeJS](https://nodejs.org/en/download/)
- [React-native](https://facebook.github.io/react-native/docs/getting-started)
- **XCode**<sup>[1]</sup>, ou o **Android Studio**<sup>[2]</sup>

Primeiro, baixar e instalar o NodeJS na versão LTS.
Em seguida, rodar o comando `npm install -g react-native-cli` no terminal para instalar o comando globalmente do react-native.

<details>
<summary>Instalação para dispositivos iOS</summary>

Após a conclusão do comando anterior, deve-se baixar e instalar o xcode<sup>\*</sup> com o `command-line tools` instalado.

<sup>\*</sup> Para mais informações, abrir a documentação do react-native abaixo.

</details>

<details>
<summary>Instalação para dispositivos Android</summary>

Após a conclusão do comando anterior, é necessário baixar e instalar o Android Studio e também criar um emulador<sup>\*</sup>.

<sup>\*</sup> Para mais informações, abrir a documentação do react-native abaixo.

</details>

[Documentação do React-Native](https://facebook.github.io/react-native/)

> <sup>[1]</sup> Somente para máquinas Mac

> <sup>[2]</sup> Para máquinas Mac/Windows/Linux

### COMO RODAR

Após obter o repositório do projeto, por clone (com o comando `git clone https://gitlab.com/filipepiresg/sunnyDay`) ou download, deverá ser feito o download das dependências do projeto através do comando `npm install` no terminal.

Se tudo estiver dado certo, o projeto já deve estar funcionando perfeitamente e para rodar tem que utilizar o comando `npx react-native run-ios` ou `npx react-native run-android` para dispositivos iOS e android, respectivamente.

 <p style="color:red; text-align:center;font-weight:bold">Caso seja utilizado o android, é necessário abrir o emulador antes de rodar o projeto</p>

Após ter instalado no dispositivo, pela primeira vez, não é necessário rodar esse comando (pois demora demais) podemos simplesmente rodar o comando `npx react-native start` para que o bundler inicie rapidamente.

### SCREENSHOTS

Segue alguns screenshots de dispositivos da Apple e Android:

![iPhone 11](screenshots/iPhone11-2.png "iPhone 11")

> Resolução 640x1136

![Android Pixel2](screenshots/AndroidPixel2.png "Android Pixel 2")

> Resolução 828x1792

### PROPRIEDADES

<details>

- `#FFFFFF` (COR TEXTO)
- `#1CABED`(COR PRIMARIA)
- `#FFFFFFAA` (COR BOTÃO REFRESH)

- `Roboto` (Família fonte)
- [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons) (PACOTE DE ICONES)
- `Reactotron`
- `date-fns`
- `native-base`
- `hooks`
- `lodash`
- `typescript`
- `npx react-native --version` = `react-native: 0.63.0`

</details>

---
