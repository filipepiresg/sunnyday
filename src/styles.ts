import { StyleSheet } from 'react-native';

import { opacify } from 'polished';
import colors from './styles/colors';

export default StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: colors.PRIMARY,
  },
  container: {
    backgroundColor: colors.PRIMARY,
    padding: 10,
    flex: 1,
  },
  title: {
    color: colors.WHITE,
    fontSize: 40,
    fontFamily: 'Roboto',
  },
  subtitle: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: 'Roboto',
  },
  imageIcon: {
    width: 50,
    height: 50,
    tintColor: '#fff',
    marginTop: 10,
  },
  footer: {
    backgroundColor: colors.PRIMARY,
    borderTopWidth: 0,
    justifyContent: 'center',
  },
  footerText: {
    color: 'white',
    paddingHorizontal: 10,
    fontSize: 25,
    fontFamily: 'Roboto',
    paddingTop: 10,
  },
  containerScrollCard: {
    alignItems: 'flex-end',
    paddingHorizontal: 5,
  },
  scrollCard: {
    alignSelf: 'flex-end',
  },
  icon: {
    color: opacify(-0.3, colors.WHITE),
    marginRight: 10,
  },
  containerCentered: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colors.WHITE,
    width: 250,
    height: 250,
    borderRadius: 8,
  },
  text: {
    color: colors.WHITE,
    fontFamily: 'Roboto',
  },
  dayText: {
    color: colors.WHITE,
    fontSize: 20,
    fontWeight: '600',
    fontFamily: 'Roboto',
  },
  image: {
    height: 40,
    width: 40,
  },
  card: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: opacify(-0.8, colors.WHITE),
    marginHorizontal: 5,
    padding: 10,
    borderRadius: 10,
    width: 150,
  },
});
