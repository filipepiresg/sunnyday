import CLEAR_DAY_SUN from '../icons/weather/004-sun.png';
import FEW_CLOUDS_SUN from '../icons/weather/003-clouds_and_sun.png';
import SCATTERED_CLOUDS_SUN from '../icons/weather/001-cloudy.png';
import BROKEN_CLOUDS_SUN from '../icons/weather/007-rain.png';
import SHOWER_RAIN_SUN from '../icons/weather/008-heavy_rain.png';
import RAIN_SUN from '../icons/weather/010-rain.png';
import THUNDERSTORM_SUN from '../icons/weather/006-thunderstorm.png';
import SNOW_SUN from '../icons/weather/011-snow.png';
import MIST_SUN from '../icons/weather/022-fog.png';

import CLEAR_DAY_NIGHT from '../icons/weather/moon.png';
import FEW_CLOUDS_NIGHT from '../icons/weather/002-cloudy_night.png';
import SCATTERED_CLOUDS_NIGHT from '../icons/weather/001-cloudy.png';
import BROKEN_CLOUDS_NIGHT from '../icons/weather/007-rain.png';
import SHOWER_RAIN_NIGHT from '../icons/weather/008-heavy_rain.png';
import RAIN_NIGHT from '../icons/weather/014-rain.png';
import THUNDERSTORM_NIGHT from '../icons/weather/006-thunderstorm.png';
import SNOW_NIGHT from '../icons/weather/011-snow.png';
import MIST_NIGHT from '../icons/weather/023-fog.png';

export default {
  CLEAR_DAY_SUN,
  FEW_CLOUDS_SUN,
  SCATTERED_CLOUDS_SUN,
  BROKEN_CLOUDS_SUN,
  SHOWER_RAIN_SUN,
  RAIN_SUN,
  THUNDERSTORM_SUN,
  SNOW_SUN,
  MIST_SUN,
  CLEAR_DAY_NIGHT,
  FEW_CLOUDS_NIGHT,
  SCATTERED_CLOUDS_NIGHT,
  BROKEN_CLOUDS_NIGHT,
  SHOWER_RAIN_NIGHT,
  RAIN_NIGHT,
  THUNDERSTORM_NIGHT,
  SNOW_NIGHT,
  MIST_NIGHT,
};
