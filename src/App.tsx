/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Dimensions, SafeAreaView, Image, View, StatusBar, ScrollView } from 'react-native';
import RNLocation from 'react-native-location';
import Modal from 'react-native-modal';
import SplashScreen from 'react-native-splash-screen';

import('./config/ReactotronConfig');

import { format, fromUnixTime } from 'date-fns';
import pt from 'date-fns/locale/pt-BR';
import drop from 'lodash/drop';
import get from 'lodash/get';
import upperFirst from 'lodash/upperFirst';
import { Icon, Container, Card, Right, Footer, Text, Spinner } from 'native-base';
import last from 'lodash/last';

import Styles from './styles';
import weatherConditions from './styles/weatherConditions';

const { width, height } = Dimensions.get('screen');

RNLocation.configure({
  distanceFilter: 5.0,
  desiredAccuracy: {
    ios: 'best',
    android: 'balancedPowerAccuracy',
  },
});

const API_KEY = 'e2b26b62be82f2fa8fe5f2091312c2ae';

interface WeatherInterface {
  coord: Object;
  weather: Array<{
    id: number;
    main: String;
    description: String;
    icon: String;
  }>;
  base: String;
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
    sea_level: number;
    grnd_level: number;
  };
  wind: {
    speed: number;
    deg: number;
  };
  clouds: {
    all: number;
  };
  dt: String;
  sys: {
    country: String;
    sunrise: number;
    sunset: number;
    timezone: number;
  };
  timezone: number;
  id: number;
  name: String;
  cod: number;
}

interface DailyWeather {
  dt: number;
  weather: Array<{
    id: number;
    main: String;
    description: String;
    icon: String;
  }>;
}

const App: React.FC = () => {
  const [isLoading, setLoading] = useState(true);
  const [weather, setWeather] = useState<WeatherInterface>();
  const [weathers, setWeathers] = useState<DailyWeather[]>([]);

  const refreshWeather = useCallback(async (lat: Number, lng: Number) => {
    let isLoadingWeather = false;
    let isLoadingWeathers = false;
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&APPID=${API_KEY}&units=metric`
    )
      .then((res) => res.json())
      .then((res) => {
        setWeather(res);
      })
      .finally(() => {
        isLoadingWeather = true;
        if (isLoadingWeathers) {
          setLoading(false);
        }
      });

    // fetch(
    //   `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&appid=${API_KEY}&exclude=hourly&units=metric`
    // )
    fetch(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&appid=${API_KEY}&exclude=hourly,minutely,current&units=metric`
    )
      .then((res) => res.json())
      .then((res) => {
        const _weathers = drop<DailyWeather>(res.daily);
        setWeathers(_weathers);
      })
      .finally(() => {
        isLoadingWeathers = true;
        if (isLoadingWeather) {
          setLoading(false);
        }
      });
  }, []);

  const getLocation = useCallback(() => {
    setLoading(true);
    RNLocation.getLatestLocation({ timeout: 1 * 1000 })
      .then((locations) => {
        refreshWeather(
          get(locations, 'latitude', -15.7744219),
          get(locations, 'longitude', -48.0779749)
        );
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    RNLocation.requestPermission({
      ios: 'whenInUse',
      android: {
        detail: 'coarse',
      },
    })
      .then((granted) => {
        if (granted) {
          getLocation();
        }
      })
      .finally(() => {
        SplashScreen.hide();
      });
  }, []);

  const dayTime = useMemo(() => {
    const _last = last(get(weather, 'weather[0].icon', ''));
    if (_last === 'n') {
      return 'night';
    }
    return 'day';
  }, [weather]);

  return (
    <SafeAreaView style={Styles.safeArea}>
      <StatusBar
        backgroundColor={Styles.container.backgroundColor}
        animated
        barStyle='light-content'
      />
      <Container style={Styles.container}>
        <Card transparent>
          <Text style={Styles.title}>{get(weather, 'main.temp', 0).toFixed(0)}°</Text>
          <Text style={Styles.subtitle}>
            {`Sensação térmica ${get(weather, 'main.feels_like', 0).toFixed(0)}`}°
          </Text>
          <Image
            style={Styles.imageIcon}
            source={
              weatherConditions[get(weather, 'weather[0].description', 'clear sky')][dayTime].icon
            }
          />
        </Card>

        {/* <Text style={[Styles.dayText, { paddingLeft: 10 }]}>Próximos dias</Text> */}
      </Container>
      <View>
        <Text style={[Styles.dayText, { paddingLeft: 10 }]}>Próximos dias</Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={Styles.containerScrollCard}
          style={Styles.scrollCard}
        >
          {weathers.map((item) => (
            <View style={Styles.card} key={String(item.dt)}>
              <Text style={Styles.dayText}>
                {upperFirst(format(fromUnixTime(item.dt), 'EEEE', { locale: pt }))}
              </Text>
              <Image
                style={Styles.image}
                source={{
                  uri: `http://openweathermap.org/img/wn/${item?.weather[0].icon}@2x.png`,
                }}
              />
              <Text style={Styles.text}>min {get(item, 'temp.min', 0).toFixed(0)}°</Text>
              <Text style={Styles.text}>max {get(item, 'temp.max', 0).toFixed(0)}°</Text>
            </View>
          ))}
        </ScrollView>
      </View>

      <Footer style={Styles.footer}>
        <Text style={Styles.footerText}>{get(weather, 'name', '')}</Text>
        <Right>
          <Icon
            name='refresh'
            style={Styles.icon}
            onPress={() => {
              RNLocation.checkPermission({
                ios: 'whenInUse',
                android: {
                  detail: 'coarse',
                },
              }).then((granted) => {
                if (granted) {
                  getLocation();
                }
              });
            }}
          />
        </Right>
      </Footer>

      <Modal
        backdropColor={Styles.safeArea.backgroundColor}
        isVisible={isLoading}
        deviceHeight={height}
        deviceWidth={width}
        animationIn='fadeIn'
        animationOut='fadeOut'
      >
        <View style={Styles.containerCentered}>
          <Spinner size='large' color={Styles.safeArea.backgroundColor} />
        </View>
      </Modal>
    </SafeAreaView>
  );
};

export default App;
