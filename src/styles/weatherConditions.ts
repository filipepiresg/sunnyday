import { Icons } from '../assets';

export default {
  'clear sky': {
    day: {
      icon: Icons.CLEAR_DAY_SUN,
    },
    night: {
      icon: Icons.CLEAR_DAY_NIGHT,
    },
  },
  'few clouds': {
    day: {
      icon: Icons.FEW_CLOUDS_SUN,
    },
    night: {
      icon: Icons.FEW_CLOUDS_NIGHT,
    },
  },
  'scattered clouds': {
    day: {
      icon: Icons.SCATTERED_CLOUDS_SUN,
    },
    night: {
      icon: Icons.SCATTERED_CLOUDS_NIGHT,
    },
  },
  'broken clouds': {
    day: {
      icon: Icons.BROKEN_CLOUDS_SUN,
    },
    night: {
      icon: Icons.BROKEN_CLOUDS_NIGHT,
    },
  },
  'shower rain': {
    day: {
      icon: Icons.SHOWER_RAIN_SUN,
    },
    night: {
      icon: Icons.SHOWER_RAIN_NIGHT,
    },
  },
  rain: {
    day: {
      icon: Icons.RAIN_SUN,
    },
    night: {
      icon: Icons.RAIN_NIGHT,
    },
  },
  thunderstorm: {
    day: {
      icon: Icons.THUNDERSTORM_SUN,
    },
    night: {
      icon: Icons.THUNDERSTORM_NIGHT,
    },
  },
  snow: {
    day: {
      icon: Icons.SNOW_SUN,
    },
    night: {
      icon: Icons.SNOW_NIGHT,
    },
  },
  mist: {
    day: {
      icon: Icons.MIST_SUN,
    },
    night: {
      icon: Icons.MIST_NIGHT,
    },
  },
  '': {
    day: {
      icon: Icons.CLEAR_DAY_SUN,
    },
    night: {
      icon: Icons.CLEAR_DAY_NIGHT,
    },
  },
};
