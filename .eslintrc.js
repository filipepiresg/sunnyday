module.exports = {
  root: true,
  extends: ['@react-native-community'],
  plugins: ['@typescript-eslint'],
  parser: '@typescript-eslint/parser',
};
